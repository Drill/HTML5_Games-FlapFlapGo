# Flap Flap Go
Dodge obstacles and aim for the highest score!<br/>
In this game there are 12 misterious achivements. Will you be able to find out them all?
In addition, obtain all achivements to unlock a new feature!
</br></br>
You can play both on PC and on Smartphone (better experience on PC). On Smartphone you need to use a browser and not a HTML viewer.
<br/><br/>
Try it online at https://drill.gitlab.io/HTML5_Games-FlapFlapGo
</br></br>

## Commands
There are 3 types of commands:
<ul>
  <li><b>Keyboard</b>: use the spacebar to move the sprite.</li>
  <li><b>Button</b>: click/tap the button to move the sprite.</li>
  <li><b>Touch</b>: touch the game area to move the sprite.</li>
</ul>
</br>

## Screenshots
![FlapFlapGo_screenshot1](https://gitlab.com/Drill/HTML5_Games-FlapFlapGo/uploads/1c7d9a3e38bd0d597da49258420155ea/FlapFlapGo_screenshot1.PNG)
![FlapFlapGo_screenshot2](https://gitlab.com/Drill/HTML5_Games-FlapFlapGo/uploads/b4cb62b3046f32af204518cca43f6db2/FlapFlapGo_screenshot2.PNG)
<br/><br/>

## Credits
Thanks to Fabio Prizio to have drawn the sprite, the flame and the sprite's broken layer!<br/>
Check out the `.html` file to see all other credits.
<br/>
<br/>

---


<i>Note</i>: if you change the files position, you'll need to update the relative paths in the `.html` file.